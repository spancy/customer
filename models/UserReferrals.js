const Knex = require("knex");
const connection = require("../knexfile");
const { Model } = require("objection");

const knexConnection = Knex(connection.development);

Model.knex(knexConnection);
//Adding models
class UserReferrals extends Model {
  static get tableName() {
    return "userReferrals";
  }
}

module.exports = UserReferrals;
