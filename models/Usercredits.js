const Knex = require("knex");
const connection = require("../knexfile");
const { Model } = require("objection");

const knexConnection = Knex(connection.development);

Model.knex(knexConnection);
//Adding modelx
class Usercredits extends Model {
  static get tableName() {
    return "userCredits";
  }

  static get relationMappings() {
    return {
      employee: {
        relation: Model.BelongsToOneRelation,
        modelClass: __dirname + "/Customer",
        join: {
          from: "userCredits.customer_id",
          to: "customer_authentication.id"
        }
      },
      interest: {
        relation: Model.BelongsToOneRelation,
        modelClass: __dirname + "/Credits",
        join: {
          from: "userCredits.credit_id",
          to: "credits.id"
        }
      }
    };
  }
}

module.exports = Usercredits;
