const Knex = require("knex");
const connection = require("../knexfile");
const { Model } = require("objection");

const knexConnection = Knex(connection.development);

Model.knex(knexConnection);
//Adding models
class Credits extends Model {
  static get tableName() {
    return "credits";
  }
}

module.exports = Credits;
