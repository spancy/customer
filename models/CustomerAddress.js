"use strict";

const Model = require("objection").Model;
const knex = require("knex");

const KnexConfig = require("../knexfile");

Model.knex(knex(KnexConfig.development));
class CustomerAddress extends Model {
  // Table name is the only required property.
  static get tableName() {
    return "customer_address";
  }

  static get relationMappings() {
    return {
      customer: {
        relation: Model.BelongsToOneRelation,
        // The related model. This can be either a Model subclass constructor or an
        // absolute file path to a module that exports one. We use the file path version
        // here to prevent require loops.
        modelClass: __dirname + "/Customer",
        join: {
          from: "customer_address.customer_id",
          to: "customer_authentication.id"
        }
      }
    };
  }
}
module.exports = CustomerAddress;
