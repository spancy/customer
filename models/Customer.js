"use strict";

const Model = require("objection").Model;
const knex = require("knex");

const KnexConfig = require("../knexfile");

Model.knex(knex(KnexConfig.development));
class Customer extends Model {
  // Table name is the only required property.
  static get tableName() {
    return "customer_authentication";
  }

  static get relationMappings() {
    return {
      profile: {
        relation: Model.HasManyRelation,
        // The related model. This can be either a Model subclass constructor or an
        // absolute file path to a module that exports one. We use the file path version
        // here to prevent require loops.
        modelClass: __dirname + "/CustomerProfile",
        join: {
          from: "customer_profile.customer_id",
          to: "customer_authentication.id"
        }
      },
      address: {
        relation: Model.HasManyRelation,
        // The related model. This can be either a Model subclass constructor or an
        // absolute file path to a module that exports one. We use the file path version
        // here to prevent require loops.
        modelClass: __dirname + "/CustomerAddress",
        join: {
          from: "customer_address.customer_id",
          to: "customer_authentication.id"
        }
      },
      email: {
        relation: Model.HasManyRelation,
        // The related model. This can be either a Model subclass constructor or an
        // absolute file path to a module that exports one. We use the file path version
        // here to prevent require loops.
        modelClass: __dirname + "/CustomerEmail",
        join: {
          from: "customer_email.customer_id",
          to: "customer_authentication.id"
        }
      },
      phone: {
        relation: Model.HasManyRelation,
        // The related model. This can be either a Model subclass constructor or an
        // absolute file path to a module that exports one. We use the file path version
        // here to prevent require loops.
        modelClass: __dirname + "/CustomerPhone",
        join: {
          from: "customer_phone.customer_id",
          to: "customer_authentication.id"
        }
      },
      payments: {
        relation: Model.HasManyRelation,
        // The related model. This can be either a Model subclass constructor or an
        // absolute file path to a module that exports one. We use the file path version
        // here to prevent require loops.
        modelClass: __dirname + "/CustomerPayments",
        join: {
          from: "customer_payment_details.customer_id",
          to: "customer_authentication.id"
        }
      },
      kycDetails: {
        relation: Model.HasManyRelation,
        // The related model. This can be either a Model subclass constructor or an
        // absolute file path to a module that exports one. We use the file path version
        // here to prevent require loops.
        modelClass: __dirname + "/CustomerKYCDetails",
        join: {
          from: "customer_kyc_details.customer_id",
          to: "customer_authentication.id"
        }
      }
    };
  }
}
module.exports = Customer;
