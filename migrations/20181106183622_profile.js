exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema
      .createTable("customer_authentication", function(table) {
        table.increments("id").primary();
        table
          .boolean("is_individual")
          .notNullable()
          .defaultTo(true);
        table.string("user_name");
        table.string("password");
        table.string("registered_email");
        table.string("phone_number");
        table.boolean("is_otp_verified");
        table.boolean("is_email_verified");
        table.string("email_verification_hash");
        table.string("otp_hash");
        table.string("reset_password_hash");
        table.date("password_last_modified");
        table
          .boolean("is_active")
          .notNullable()
          .defaultTo(true);
        table.timestamps(true, true);
      })

      .createTable("customer_email", function(table) {
        table.increments("id").primary();
        table
          .integer("customer_id")
          .unsigned()
          .references("customer_authentication.id");
        table.string("description");
        table.string("email_address");
        table
          .boolean("verified")
          .notNullable()
          .defaultTo(false);
        table
          .boolean("reserved")
          .notNullable()
          .defaultTo(false);
        table
          .boolean("email_comm_ok")
          .notNullable()
          .defaultTo(true);
        table.timestamps(true, true);
      })
      .createTable("customer_address", function(table) {
        table.increments("id").primary();
        table
          .integer("customer_id")
          .unsigned()
          .references("customer_authentication.id");
        table.string("full_name");
        table.string("line1");
        table.string("line2");
        table.string("city");
        table.string("state");
        table.string("zip_code");
        table.string("country");
        table
          .boolean("verified")
          .notNullable()
          .defaultTo(false);
        table
          .boolean("reserved")
          .notNullable()
          .defaultTo(false);
        table.timestamps(true, true);
      })
      .createTable("customer_phone", function(table) {
        table.increments("id").primary();
        table
          .integer("customer_id")
          .unsigned()
          .references("customer_authentication.id");
        table
          .string("number")
          .unique()
          .notNullable();
        table.string("country_code");
        table.integer("last4");
        table.enu("type", ["home", "mobile", "work"]);
        table
          .boolean("verified")
          .notNullable()
          .defaultTo(false);
        table
          .boolean("reserved")
          .notNullable()
          .defaultTo(false);
        table.timestamps(true, true);
      })
      .createTable("customer_kyc_details", function(table) {
        table.increments("id").primary();
        table
          .integer("customer_id")
          .unsigned()
          .references("customer_authentication.id");
        table.string("document_type");
        table.string("document_unique_id");
        table.string("description");
        table.integer("document_id");
        table
          .boolean("verified")
          .notNullable()
          .defaultTo(false);
        table.timestamps(true, true);
      })
      .createTable("customer_payment_details", function(table) {
        table.increments("id").primary();
        table
          .integer("customer_id")
          .unsigned()
          .references("customer_authentication.id");
        table.string("payment_type");
        table.string("payment_unique_id");
        table.string("description");
        table.integer("document_id");
        table.string("fund_account_id");
        table
          .boolean("verified")
          .notNullable()
          .defaultTo(false);
      table.timestamps(true, true);
      })
      .createTable("referrals", function(table) {
        table.increments("id").primary();
        table.string("customer_id");
        table.string("referral_code");
        table.unique(["customer_id", "referral_code"]);
        table.string("free_task_count");
        table.boolean("max_used").defaultTo("false");
        table.timestamps(true, true);
      })
      .createTable("userReferrals", function(table) {
        table.increments("id").primary();
        table.string("referral_id");
        table.string("referred_to").unique();
        table.boolean("referee_subscribed").defaultTo("false");
        table.string("referee_task_count");
        table.timestamps(true, true);
      })
      .createTable("credits", function(table) {
        table.increments("id").primary();
        table.string("name");
        table.integer("maxCredit");
        table.string("description");
        table.integer("cost");
        table.integer("expirationPolicy");
        table.timestamps(true, true);
      })
      .createTable("userCredits", function(table) {
        table.increments("id").primary();
        table.integer("customer_id").references("customer_authentication.id");
        table.integer("credit_id").references("credits.id");
        table.date("activation_date");
        table.date("expiration_date");
        table.integer("initial_credits");
        table.integer("remaining_credits");
        table.integer("spent_credits");
      table.timestamps(true, true);
      })
      .createTable("userroles", function(table) {
        table.increments("id").primary();
        table
          .string("role")
          .unique()
          .notNullable();
        table.string("description");
        table.timestamps(true, true);
      })
      .createTable("emailcategory", function(table) {
        table.increments("id").primary();
        table
          .string("category")
          .unique()
          .notNullable();
        table.string("description");
        table.timestamps(true, true);
      })
      .createTable("emailpreferences", function(table) {
        table.increments("id").primary();
        table
          .integer("email_id")
          .unsigned()
          .references("customer_email.id");
        table
          .integer("email_category_id")
          .unsigned()
          .references("emailcategory.id");
        table
          .boolean("comm_ok")
          .notNullable()
          .defaultTo(true);
        table.unique(["email_id", "email_category_id"]);
        table.timestamps(true, true);
      })
      .createTable("customer_preferences", function(table) {
        table.increments("id").primary();
        table
          .integer("customer_id")
          .unsigned()
          .references("customer_authentication.id");
        table
          .integer("default_billing_address_id")
          .unsigned()
          .references("customer_address.id");
        table
          .integer("default_shipping_address_id")
          .unsigned()
          .references("customer_address.id");
        table
          .integer("primary_address_id")
          .unsigned()
          .references("customer_address.id");
        table
          .integer("primary_phone_id")
          .unsigned()
          .references("customer_phone.id");
        table
          .integer("primary_email_id")
          .unsigned()
          .references("customer_email.id");
        table
          .boolean("email_comm_ok")
          .notNullable()
          .defaultTo(true);
        table
          .boolean("phone_comm_ok")
          .notNullable()
          .defaultTo(true);

        table.timestamps(true, true);
      })
      .createTable("customer_profile", function(table) {
        table.increments("id").primary();
        table.integer("customer_id").references("customer_authentication.id");
        table.integer("primary_email_id").references("customer_email.id");
        table.integer("primary_phone_id").references("customer_phone.id");
        table.integer("primary_address_id").references("customer_address.id");
        table.string("display_image_url");
        table
          .boolean("is_individual")
          .notNullable()
          .defaultTo(true);
        table.enu("customer_gender", ["M", "F", "NA"]);
        table.boolean("is_account_verified");
        table.boolean("is_kyc_verified");
        table.boolean("is_email_verified");
        table.boolean("is_referred").defaultTo(false);
        table.string("first_name");
        table.string("last_name");
        table.date("date_of_birth");
        table.string("user_name");
        table.string("organization_name");
        table.string("contact_id");
        table.boolean("new_user").defaultTo(true);
        table
          .boolean("is_active")
          .notNullable()
          .defaultTo(true);
        table.timestamps(true, true);
      })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTableIfExists("customer_preferences"),
    knex.schema.dropTableIfExists("emailpreferences"),
    knex.schema.dropTableIfExists("emailcategory"),
    knex.schema.dropTableIfExists("userroles"),
    knex.schema.dropTableIfExists("customer_payment_details"),
    knex.schema.dropTableIfExists("customer_kyc_details"),
    knex.schema.dropTableIfExists("customer_phone"),
    knex.schema.dropTableIfExists("customer_address"),
    knex.schema.dropTableIfExists("customer_email"),
    knex.schema.dropTableIfExists("customer_profile"),
    knex.schema.dropTableIfExists("customer_authentication")
  ]);
};
