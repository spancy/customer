var express = require("express");
var router = express.Router();

var postHelper = require("../helpers/customerPostHelper");
var getHelper = require("../helpers/customerGetHelper");
var putHelper = require("../helpers/customerPutHelper");

const knex = require("knex");

router
  .route("/")

  //gets called when a new template is added/ a task is created for the first time since it requires the relation to be mapped
  .post(async function(req, res) {
    let response = await postHelper.customerInsert(req);
    res.json(response);
  });

  router
    .route("/sendEmail")
    .post(async function(req, res) {
      let response = await postHelper.sendInvoiceSes(req);
      res.json(response);
    });

router
  .route("/:customer_id")

  //gets called when a new template is added/ a task is created for the first time since it requires the relation to be mapped
  .put(async function(req, res) {
    let response = await putHelper.customerUpdate(req);
    res.json(response);
  });

router
  .route("/all")
  // fetch all addresses
  .get(async function(req, res) {
    let response = await getHelper.customerGetAllData(req);
    res.json(response);
  });

  router
    .route("/getByEmailID/:registered_email")
    // fetch all addresses
    .get(async function(req, res) {
      let response = await getHelper.customerGetData(req, "registered_email");
      res.json(response);
    });


    router
    .route("/getByCustomerID/:phone_number")
    // fetch all addresses
    .get(async function(req, res) {
      let response = await getHelper.customerGetData(req, "phone_number");
      res.json(response);
    });

router
  .route("/getByID/:id")
  // fetch all addresses
  .get(async function(req, res) {
    let response = await getHelper.customerGetData(req, "id");
    res.json(response);
  });
module.exports = router;
