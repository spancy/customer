var express = require("express");
var router = express.Router();

var postHelper = require("../helpers/customerPostHelper");
var getHelper = require("../helpers/customerGetHelper");
var putHelper = require("../helpers/customerPutHelper");

const knex = require("knex");

router.route("/").post(async function(req, res) {
  let response = await postHelper.userCreditsInsert(req);
  res.json(response);
});

router.route("/:id").put(async function(req, res) {
  let response = await putHelper.userCreditsUpdate(req);
  res.json(response);
});

router.route("/customer/customer_id").put(async function(req, res) {
  let response = await putHelper.userCreditsUpdateBycustomerID(req);
  res.json(response);
});

/*router.route("/").get(async function(req, res) {
  let response = await getHelper.employeeAddressAllData(req);
  res.json(response);
});*/

router.route("/getByID/:id").get(async function(req, res) {
  let response = await getHelper.customerGetUserCreditsData(req, "id");
  res.json(response);
});

router.route("/getBycustomerID/:customer_id").get(async function(req, res) {
  let response = await getHelper.customerGetUserCreditsData(req, "customer_id");
  res.json(response);
});

module.exports = router;
