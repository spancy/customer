//POST - {status : status, message : message}
// status - 200 if everything is fine
// status - 500 if any server error occurs
// message corresponds to the error, or "Reset Password link sent to user" if everything succeeds


var express = require('express');
var UserAuthentication = require('../db/models/userAuthentication');
var configFile = require('./config')

var router = express.Router();

var crypto = require('crypto')
var promise = require('bluebird')
var mailer = require('nodemailer-promise');

var resetPasswordHash = ''
var status = ''
var message = ''
var sendPasswordEmail = ''

var resetPasswordHashGenerationFlag = false
var dataUpdationFlag = false
var sendMailFlag = false

router.post('/', function(req,res, next){

	var server = req.get('host')

	sendPasswordEmail = req.body.email

	var hashGenerationPromise = promise.promisify(crypto.randomBytes)
				hashGenerationPromise(configFile.bytesForHash).then(function(buffer){

					return buffer.toString('hex')

				}).then(function(result) {

					resetPasswordHashGenerationFlag = true

					resetPasswordHash = result

					console.log(resetPasswordHash)
					console.log(sendPasswordEmail)

					return UserAuthentication.where({registeredEmail:sendPasswordEmail}).save({resetPasswordHash: result},{patch:true})

				}).then(function(result){
					UserAuthentication.where({registeredEmail:sendPasswordEmail}).fetch()
					.then(function(result){
var userDetails =result.toJSON();
						console.log(userDetails)

							dataUpdationFlag = true

								var sendEmail = mailer.config({
									email: configFile.senderMail,
									password: configFile.senderMailPassword,
									server: configFile.senderServer
							});

							var options = {
									subject: 'UrbanHealth - Reset Password link',
									senderName: configFile.senderName,
									receiver: req.body.email,
									html: '<!DOCTYPE html><html>\
									<body>\
									<p>Click on the <a href=\"http://www.urbanhealth.co.in/#/resetpassword?username='+userDetails.username+'&key=' + resetPasswordHash + '\">link</a> to reset your password.</p>\
									</br>\
									</body>\
									</html>'
							};

							return sendEmail(options)




				}).then(function(result){

					sendMailFlag = true

					status = 200
					message = "Reset Password link sent to user"

				}).catch(function(error){

					if(!resetPasswordHashGenerationFlag){
						status = 500
						message = "Reset Password Hash generation error"
					}

					if(resetPasswordHashGenerationFlag && !dataUpdationFlag && !sendMailFlag){
						status = 500
						message = "Data Updation error"
					}
					if(resetPasswordHashGenerationFlag && dataUpdationFlag && !sendMailFlag){
						status = 500
						message = "Mail error"
					}
				})
}).then(function(result){

	sendMailFlag = true

	status = 200
	message = "Reset Password link sent to user"

})
				.finally(function(){

					res.json({status:status,message:message})
				})
});

module.exports = router;
