var express = require("express");
var router = express.Router();

var postHelper = require("../helpers/customerPostHelper");
var getHelper = require("../helpers/customerGetHelper");
var putHelper = require("../helpers/customerPutHelper");

const knex = require("knex");

router
  .route("/")
  // fetch all addresses
  .post(async function(req, res) {
    let response = await postHelper.customerProfileInsert(req);
    res.json(response);
  });

router
  .route("/:customer_profile_id")

  //gets called when a new template is added/ a task is created for the first time since it requires the relation to be mapped
  .put(async function(req, res) {
    let response = await putHelper.customerProfileUpdate(req);
    res.json(response);
  });

  router.route("/customerID/:customer_id").put(async function(req, res) {
    let response = await putHelper.customerProfilePut(req);
    res.json(response);
  });

router
  .route("/getByID/:id")
  // fetch all addresses
  .get(async function(req, res) {
    let response = await getHelper.customerGetProfileData(req, "id");
    res.json(response);
  });

  router.route("/razorpaycontact/").post(async function(req, res) {
    let response = await postHelper.razorpayPostContact(req);
    res.json(response);
    console.log(response);
  });

  router.route("/razorpayfund/bank").post(async function(req, res) {
    let response = await postHelper.razorpayPostFundAccBank(req);
    res.json(response);
    console.log(response);
  });

  router.route("/razorpayfund/vpa").post(async function(req, res) {
    let response = await postHelper.razorpayPostFundAccVpa(req);
    res.json(response);
    console.log(response);
  });

  router.route("/razorpaycontact/:employee_id").put(async function(req, res) {
    let response = await putHelper.razorpayPutContact(req);
    res.json(response);
  });

router
  .route("/getBycustomerID/:id")
  // fetch all addresses
  .get(async function(req, res) {
    let response = await getHelper.customerGetProfileData(req, "customer_id");
    res.json(response);
  });

module.exports = router;
