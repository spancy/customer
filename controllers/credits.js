var express = require("express");
var router = express.Router();

var postHelper = require("../helpers/customerPostHelper");
var getHelper = require("../helpers/customerGetHelper");
var putHelper = require("../helpers/customerPutHelper");

const knex = require("knex");

router.route("/").post(async function(req, res) {
  let response = await postHelper.creditsInsert(req);
  res.json(response);
});

router.route("/:id").put(async function(req, res) {
  let response = await putHelper.creditsUpdate(req);
  res.json(response);
});

router.route("/").get(async function(req, res) {
  let response = await getHelper.customercreditsAllData(req);
  res.json(response);
});

router.route("/getByID/:id").get(async function(req, res) {
  let response = await getHelper.customerGetCreditsData(req, "id");
  res.json(response);
});

module.exports = router;
