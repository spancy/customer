//GET - {status : status, message : message, username : username }
// status - 200 if everything is fine
// status - 500 if any server error occurs
// message - "Success" or "Failure" if everything succeeds
// username - a string that represents the username of the user whose password needs to be reset.
//POST - {status : status, message : message}
// status - 200 if everything is fine
// status - 500 if any server error occurs
// message - "Success" or "Failure" if everything succeeds

var express = require('express');
var UserAuthentication = require('../db/models/userAuthentication');
var configFile = require('./config')

var router = express.Router();

var bcrypt = require('bcrypt-nodejs')

//access the reset password hash from the URL
router.get('/', function(req,res, next){

	var resetPasswordHashValue = req.query.key

	new UserAuthentication({'resetPasswordHash': resetPasswordHashValue})
  	.fetch()
  	.then(function(result) {
  		var username = result.get('username')
    	res.json({status:200,message:"Success",username:username})
	}).catch(function(error){
		res.json({status:500,message:"Failure",username:''})
	});
});

//set the new password
router.post('/', function(req,res, next){

	var newPassword = req.body.password
//var salt = bcrypt.genSaltSync(12)
//console.log("salt ",salt)
	var result = bcrypt.hashSync(newPassword, bcrypt.genSaltSync(12))
//	.then(function(result) {
     console.log("result",result)
    //	return UserAuthentication.where({username:req.body.username}).save({password: result},{patch:true})

	//})
	UserAuthentication.where({username:req.body.username}).save({password: result},{patch:true})
	.then(function(result) {

      res.json({status:200,message:"Success"})

    }).catch(function(error){

    	res.json({status:500,message:"Failure"})

    });;

});

module.exports = router
