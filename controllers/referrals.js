var express = require("express");
var router = express.Router();

var postHelper = require("../helpers/customerPostHelper");
var getHelper = require("../helpers/customerGetHelper");
var putHelper = require("../helpers/customerPutHelper");

const knex = require("knex");

router.route("/").post(async function(req, res) {
  let response = await postHelper.referralsInsert(req);
  res.json(response);
});

router.route("/generate").post(async function(req, res) {
  let response = await postHelper.referralGenerator(req);
  res.json(response);
});

router.route("/:id").put(async function(req, res) {
  let response = await putHelper.customerReferralsUpdate(req);
  res.json(response);
});

router.route("/putBycustomerID/:id").put(async function(req, res) {
  let response = await putHelper.customerReferralsUpdateBycustomer(req);
  res.json(response);
});

router.route("/").get(async function(req, res) {
  let response = await getHelper.customerreferralsAllData(req);
  res.json(response);
});

router.route("/getByID/:referral_code").get(async function(req, res) {
  let response = await getHelper.customerGetReferralsData(req, "referral_code");
  res.json(response);
});

router.route("/getBycustomerID/:customer_id").get(async function(req, res) {
  let response = await getHelper.customerGetReferralsData(req, "customer_id");
  res.json(response);
});

router.route("/getByReferralID/:id").get(async function(req, res) {
  let response = await getHelper.customerGetReferralsData(req, "id");
  res.json(response);
});

module.exports = router;
