var express = require("express");
var router = express.Router();

var postHelper = require("../helpers/customerPostHelper");
var getHelper = require("../helpers/customerGetHelper");
var putHelper = require("../helpers/customerPutHelper");

const knex = require("knex");

router
  .route("/")

  //gets called when a new template is added/ a task is created for the first time since it requires the relation to be mapped
  .post(async function(req, res) {
    let response = await postHelper.customerEmailInsert(req);
    res.json(response);
  });

router
  .route("/:customer_email_id")

  //gets called when a new template is added/ a task is created for the first time since it requires the relation to be mapped
  .put(async function(req, res) {
    let response = await putHelper.customerEmailUpdate(req);
    res.json(response);
  });

router
  .route("/getByID/:id")
  // fetch all addresses
  .get(async function(req, res) {
    let response = await getHelper.customerGetEmailData(req, "id");
    res.json(response);
  });
router
  .route("/getBycustomerID/:id")
  // fetch all addresses
  .get(async function(req, res) {
    let response = await getHelper.customerGetEmailData(req, "customer_id");
    res.json(response);
  });

module.exports = router;
