var { QueryBuilder } = require("objection");
var customer = require("../models/Customer");
var customerProfile = require("../models/CustomerProfile");
var customerAddress = require("../models/CustomerAddress");
var customerEmail = require("../models/CustomerEmail");
var customerKYCDetails = require("../models/CustomerKYCDetails");
var customerPayments = require("../models/CustomerPayments");
var customerPhone = require("../models/CustomerPhone");
var credits = require("../models/Credits");
var Referrals = require("../models/Referrals");
var UserReferrals= require("../models/UserReferrals");
var usercredits = require("../models/Usercredits");
const config=require('../config');
const axios = require("axios");
var mailer = require('nodemailer-promise');
const nodemailer = require('nodemailer');
const gigsta_config = require('../gigstaconfig');
//function to get all templates and its underlying relations
async function customerGetAllData(req) {
  try {
    const customerAllData = await customer.query().eager(
      "[profile,email,address,kycDetails,payments]"
    );

    let message = "";

    message = {
      statusCode: 200,
      body: customerAllData
    };

    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Errored : " + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

//function to get all templates and its underlying relations
async function customerGetData(req, queryBy) {
  try {
    const customerData = await customer.query()
      .where(queryBy, req.params[queryBy])
      .eager("[profile,email,address,kycDetails,payments]");

    let message = "";

    message = {
      statusCode: 200,
      body: customerData
    };

    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Errored : " + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

//function to get all templates and its underlying relations
async function customerGetAddressData(req, queryBy) {
  try {
    const customerAddressData = await customerAddress.query().where(
      queryBy,
      req.params[queryBy]
    );

    let message = "";

    message = {
      statusCode: 200,
      body: customerAddressData
    };

    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Errored : " + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

//function to get all templates and its underlying relations
async function customerGetEmailData(req, queryBy) {
  try {
    const customerEmailData = await customerEmail.query().where(
      queryBy,
      req.params[queryBy]
    );

    let message = "";

    message = {
      statusCode: 200,
      body: customerEmailData
    };

    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Errored : " + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

//function to get all templates and its underlying relations
async function customerGetProfileData(req, queryBy) {
  try {
    const customerProfileData = await customerProfile.query().where(
      queryBy,
      req.params.id
    );

    let message = "";

    message = {
      statusCode: 200,
      body: customerProfileData
    };

    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Errored : " + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

//function to get all templates and its underlying relations
async function customerGetKYCData(req, queryBy) {
  try {
    const customerKYCData = await customerKYCDetails.query().where(
      queryBy,
      req.params[queryBy]
    );

    let message = "";

    message = {
      statusCode: 200,
      body: customerKYCData
    };

    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Errored : " + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

//function to get all templates and its underlying relations
async function customerGetPaymentsData(req, queryBy) {
  try {
    const customerPaymentsData = await customerPayments.query().where(
      queryBy,
      req.params[queryBy]
    )
    .orderBy('created_at','desc');

    let message = "";

    message = {
      statusCode: 200,
      body: customerPaymentsData
    };

    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Errored : " + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

async function GetPaymentsDataBycustomerID(req, queryBy) {
  try {
    const GetPaymentsDataBycustomerID = await customerPayments.query()
    .where(
      queryBy,
      req.params.customer_id)
    .orderBy('created_at','desc');

    let message = "";

    message = {
      statusCode: 200,
      body: customerPaymentsData
    };

    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Errored : " + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

/*------------------------------ Credits -----------------------------------*/

//function to get all credits and their relations
async function customercreditsAllData(req) {
  try {
    const customercreditsAllData = await credits.query();

    let message = "";

    message = {
      statusCode: 200,
      body: customercreditsAllData
    };

    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Errored : " + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

async function customerreferralsAllData(req) {
  try {
    const customerreferralsAllData = await Referrals.query();

    let message = "";

    message = {
      statusCode: 200,
      body: customerreferralsAllData
    };

    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Errored : " + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

//function to get a address and its underlying relations
async function customerGetCreditsData(req, queryBy) {
  try {
    const customerGetCreditsData = await credits
      .query()
      .where(queryBy, req.params[queryBy]);

    let message = "";

    message = {
      statusCode: 200,
      body: customerGetCreditsData
    };

    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Errored : " + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

async function customerGetUserCreditsData(req, queryBy) {
  try {
    var today = new Date();
    today.toUTCString();
    var todate = today.getFullYear()+'/'+(today.getMonth()+1)+'/'+today.getDate();
    //console.log(today);
    const customerGetUserCreditsData = await usercredits
      .query()
      .where(queryBy, req.params[queryBy])
      //.where('expiration_date' > raw(now()))
      .orderBy('expiration_date','asc');

    let message = "";

    message = {
      statusCode: 200,
      body: customerGetUserCreditsData
    };

    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Errored : " + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

async function customerGetReferralsData(req, queryBy) {
  try {
    const customerGetReferralsData = await Referrals
      .query()
      .where(queryBy, req.params[queryBy]);

    let message = "";

    message = {
      statusCode: 200,
      body: customerGetReferralsData
    };

    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Errored : " + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

async function customerGetUserReferralsData(req, queryBy) {
  try {
    const customerGetUserReferralsData = await UserReferrals
      .query()
      .where(queryBy, req.params[queryBy]);

    let message = "";

    message = {
      statusCode: 200,
      body: customerGetUserReferralsData
    };

    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Errored : " + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}


//function to get all templates and its underlying relations
async function customerGetPhoneData(req, queryBy) {
  try {
    const customerPhoneData = await customerPhone.query().where(
      queryBy,
      req.params.id
    );

    let message = "";

    message = {
      statusCode: 200,
      body: customerPhoneData
    };

    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Errored : " + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

module.exports.customerGetAllData = customerGetAllData;
module.exports.customerGetData = customerGetData;
module.exports.customerGetProfileData = customerGetProfileData;
module.exports.customerGetPhoneData = customerGetPhoneData;
module.exports.customerGetPaymentsData = customerGetPaymentsData;
module.exports.customerGetKYCData = customerGetKYCData;
module.exports.customerGetEmailData = customerGetEmailData;
module.exports.customerGetAddressData = customerGetAddressData;
module.exports.customercreditsAllData = customercreditsAllData;
module.exports.customerGetCreditsData = customerGetCreditsData;
module.exports.customerGetUserCreditsData = customerGetUserCreditsData;
module.exports.customerGetUserReferralsData = customerGetUserReferralsData;
module.exports.customerGetReferralsData = customerGetReferralsData;
module.exports.customerreferralsAllData = customerreferralsAllData;
