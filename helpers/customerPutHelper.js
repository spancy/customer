var { QueryBuilder } = require("objection");
var customer = require("../models/Customer");
var customerProfile = require("../models/CustomerProfile");
var customerAddress = require("../models/CustomerAddress");
var customerEmail = require("../models/CustomerEmail");
var customerKYCDetails = require("../models/CustomerKYCDetails");
var customerPayments = require("../models/CustomerPayments");
var customerPhone = require("../models/CustomerPhone");
var credits = require("../models/Credits");
var Referrals = require("../models/Referrals");
var UserReferrals= require("../models/UserReferrals");
var usercredits = require("../models/Usercredits");
const config=require('../config');
const axios = require("axios");
var mailer = require('nodemailer-promise');
const nodemailer = require('nodemailer');
const gigsta_config = require('../gigstaconfig');

//function to Update into template table and relations.
async function customerUpdate(req) {
  try {
    let message = "";
    const customer1 = await customer.query()
      .update(req.body)
      .where("id", req.params.customer_id);

    message = {
      statusCode: 200,
      body: {
        message: "Updated customer Details",
        id: customer.id
      }
    };

    console.log(message);
    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Updated customer Errored" + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

async function customerProfilePut(req) {
  try {
    let message = "";
    const customerProfile1 = await customerProfile
      .query()
      .update(req.body)
      .where("customer_id", req.params.customer_id);

    message = {
      statusCode: 200,
      body: {
        message: "customer Profile Updated",
        id: customerProfile1.id
      }
    };

    console.log(message);
    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Update customer Profile Errored" + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

async function customerProfileUpdate(req) {
  try {
    let message = "";
    const customerProfile1 = await customerProfile.query()
      .update(req.body)
      .where("id", req.params.customer_profile_id);

    message = {
      statusCode: 200,
      body: {
        message: "customer Profile Updated",
        id: customerProfile1.id
      }
    };

    console.log(message);
    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Update customer Profile Errored" + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

//-------- Referrals-------
async function customerReferralsUpdate(req) {
  try {
    let message = "";
    const customerReferralsUpdate = await Referrals.query()
      .update(req.body)
      .where("id", req.params.id);

    message = {
      statusCode: 200,
      body: {
        message: "customer Referrals Updated",
        id: customerReferralsUpdate.id
      }
    };

    console.log(message);
    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Update customer Referrals Errored" + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

async function customerReferralsUpdateBycustomer(req) {
  try {
    let message = "";
    const customerReferralsUpdate = await Referrals.query()
      .update(req.body)
      .where("customer_id", req.params.id);

    message = {
      statusCode: 200,
      body: {
        message: "customer Referrals Updated",
        id: customerReferralsUpdate.id
      }
    };

    console.log(message);
    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Update customer Referrals Errored" + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

async function customerUserReferralsUpdate(req) {
  try {
    let message = "";
    const customerUserReferralsUpdate = await UserReferrals.query()
      .update(req.body)
      .where("id", req.params.id);

    message = {
      statusCode: 200,
      body: {
        message: "customer UserReferrals Updated",
        id: customerUserReferralsUpdate.id
      }
    };

    console.log(message);
    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Update customer UserReferrals Errored" + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

async function userReferralsUpdateByReferredTo(req) {
  try {
    let message = "";
    const customerUserReferralsUpdate = await UserReferrals.query()
      .update(req.body)
      .where("referred_to", req.params.id);

    message = {
      statusCode: 200,
      body: {
        message: "customer UserReferrals Updated",
        id: customerUserReferralsUpdate.id
      }
    };

    console.log(message);
    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Update customer UserReferrals Errored" + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}
//----End of referrals----

async function razorpayPutContact(payload) {
  const axios = require("axios");
  const key=config.key;
  const secret=config.secret;
  //JSON.stringify(payload);

  payload = {
    name: req.body.name,
    email: req.body.email,
    contact:req.body.contact,
    type:req.body.type
  };

  try {
    var options = {
      method:'PATCH',
      url: 'https://'+key+':'+secret+'@api.razorpay.com'+'/v1/contacts',
      data: payload
    };
    console.log(options);
    const response = await axios(options);
    console.log(response.data);
    return response.data;
  } catch (err) {
    return err;
  }
}

async function customerEmailUpdate(req) {
  try {
    let message = "";
    const customerEmail = await customerEmail.query()
      .update(req.body)
      .where("id", req.params.customer_email_id);

    message = {
      statusCode: 200,
      body: {
        message: "Updated customer Email ",
        id: customerEmail.id
      }
    };

    console.log(message);
    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Updated customer Email Errored" + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

async function customerPhoneUpdate(req) {
  try {
    let message = "";
    const customerPhone = await customerPhone.query()
      .update(req.body)
      .where("id", req.params.customer_phone_id);

    message = {
      statusCode: 200,
      body: {
        message: "Updated customer Phone ",
        id: customerPhone.id
      }
    };

    console.log(message);
    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Updated customer Phone Errored" + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

async function customerAddressUpdate(req) {
  try {
    let message = "";
    const customerAddress = await customerAddress.query()
      .update(req.body)
      .where("id", req.params.customer_address_id);

    message = {
      statusCode: 200,
      body: {
        message: "Updated customer Address ",
        id: customerAddress.id
      }
    };

    console.log(message);
    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Updated customer Address Errored" + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

async function customerKYCDetailsUpdate(req) {
  try {
    let message = "";
    const customerKYCDetails = await customerKYCDetails.query()
      .update(req.body)
      .where("id", req.params.customer_kyc_id);

    message = {
      statusCode: 200,
      body: {
        message: "Updated customer KYC Details ",
        id: customerKYCDetails.id
      }
    };

    console.log(message);
    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Updated customer KYC Details Errored" + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

async function creditsUpdate(req) {
  try {
    let message = "";
    const creditsUpdate = await credits
      .query()
      .update(req.body)
      .where("id", req.params.id);

    message = {
      statusCode: 200,
      body: {
        message: "Updated credits ",
        id: creditsUpdate.id
      }
    };

    console.log(message);
    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Updated credits Errored" + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

async function userCreditsUpdate(req) {
  try {
    let message = "";
    const userCreditsUpdate = await usercredits
      .query()
      .update(req.body)
      .where("id", req.params.id);

    message = {
      statusCode: 200,
      body: {
        message: "Updated userCredits ",
        id: userCreditsUpdate.id
      }
    };

    console.log(message);
    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Updated userCredits Errored" + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

async function userCreditsUpdateBycustomerID(req) {
  try {
    let message = "";
    const userCreditsUpdate = await usercredits
      .query()
      .update(req.body)
      .where("customer_id", req.params.customer_id);

    message = {
      statusCode: 200,
      body: {
        message: "Updated userCredits Checkboxes ",
        id: userCreditsUpdate.id
      }
    };

    console.log(message);
    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Updated userCredits Errored" + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}


async function customerPaymentsUpdate(req) {
  try {
    let message = "";
    const customerPayments = await customerPayments.query()
      .update(req.body)
      .where("id", req.params.customer_payments_id);

    message = {
      statusCode: 200,
      body: {
        message: "Updated customer Payments ",
        id: customerPayments.id
      }
    };

    console.log(message);
    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Updated customer Payments  Errored" + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

module.exports.customerUpdate = customerUpdate;
module.exports.customerProfilePut = customerProfilePut;
module.exports.razorpayPutContact = razorpayPutContact;
module.exports.customerProfileUpdate = customerProfileUpdate;
module.exports.customerPaymentsUpdate = customerPaymentsUpdate;
module.exports.customerKYCDetailsUpdate = customerKYCDetailsUpdate;
module.exports.customerAddressUpdate = customerAddressUpdate;
module.exports.customerPhoneUpdate = customerPhoneUpdate;
module.exports.customerEmailUpdate = customerEmailUpdate;
module.exports.userCreditsUpdateBycustomerID = userCreditsUpdateBycustomerID;
module.exports.userCreditsUpdate = userCreditsUpdate;
module.exports.creditsUpdate = creditsUpdate;
module.exports.customerUserReferralsUpdate = customerUserReferralsUpdate;
module.exports.customerReferralsUpdate = customerReferralsUpdate;
module.exports.customerReferralsUpdateBycustomer = customerReferralsUpdateBycustomer;
module.exports.userReferralsUpdateByReferredTo = userReferralsUpdateByReferredTo;
