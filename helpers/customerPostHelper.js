var { QueryBuilder } = require("objection");
var customer = require("../models/Customer");
var customerProfile = require("../models/CustomerProfile");
var customerAddress = require("../models/CustomerAddress");
var customerEmail = require("../models/CustomerEmail");
var customerKYCDetails = require("../models/CustomerKYCDetails");
var customerPayments = require("../models/CustomerPayments");
var customerPhone = require("../models/CustomerPhone");
var credits = require("../models/Credits");
var Referrals = require("../models/Referrals");
var UserReferrals= require("../models/UserReferrals");
var usercredits = require("../models/Usercredits");
const config=require('../config');
const axios = require("axios");
var mailer = require('nodemailer-promise');
const nodemailer = require('nodemailer');
const gigsta_config = require('../gigstaconfig');

//function to insert into template table and relations.
async function customerInsert(req) {
  const customerData = {
    is_individual: req.body.is_individual,
    user_name: req.body.user_name,
    password: req.body.password,
    registered_email: req.body.registered_email,
    phone_number: req.body.phone_number,
    is_otp_verified: req.body.is_otp_verified,
    is_email_verified: req.body.is_email_verified,
    email_verification_hash: req.body.email_verification_hash,
    otp_hash: req.body.otp_hash,
    reset_password_hash: req.body.reset_password_hash,
    password_last_modified: req.body.password_last_modified,
    is_active: req.body.is_active,
    profile: req.body.profile,
    address: req.body.address,
    email: req.body.email,
    kycDetails: req.body.kycDetails,
    payments: req.body.payments
  };

  try {
    let message = "";
    const customer1 = await customer.query().insertGraph(customerData);

    message = {
      statusCode: 200,
      body: {
        message: "New customer Created",
        id: customer1.id
      }
    };

    console.log(message);
    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "New customer Errored" + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

async function customerProfileInsert(req) {
  const customerProfileData = {
    customer_id: req.body.customer_id,
    primary_address_id: req.body.primary_address_id,
    default_shipping_address_id: req.body.default_shipping_address_id,
    display_image_url: req.body.display_image_url,
    primary_phone_id: req.body.primary_phone_id,
    primary_email_id: req.body.primary_email_id,
    is_individual: req.body.is_individual,
    customer_gender: req.body.customer_gender,
    is_account_verified: req.body.is_account_verified,
    is_kyc_verified: req.body.is_kyc_verified,
    is_email_verified: req.body.is_active,
    first_name: req.body.first_name,
    last_name: req.body.last_name,
    date_of_birth: req.body.date_of_birth,
    user_name: req.body.user_name,
    orgnaization_name: req.body.orgnaization_name,
    is_active: req.body.is_active
  };

  try {
    let message = "";
    const customerProfile1 = await customerProfile.query().insertGraph(
      customerProfileData
    );

    message = {
      statusCode: 200,
      body: {
        message: "New customer Profile Created",
        id: customerProfile1.id
      }
    };

    console.log(message);
    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "New customer Profile Errored" + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

async function razorpayPostContact(req) {
  const axios = require("axios");
  const key=config.key;
  const secret=config.secret;
  //JSON.stringify(payload);

  const payload = {
    name: req.body.name,
    email: req.body.email,
    contact:req.body.contact,
    type:req.body.type
  };


  try {
    var options = {
      method:'POST',
      url: 'https://'+key+':'+secret+'@api.razorpay.com'+'/v1/contacts',
      data: payload
    };
    //console.log(options);
    const response = await axios(options);
   console.log("Response is "+ response.data);
    return response.data;
  } catch (err) {
    return err;
  }
}
  //End of Contact API call

  //Call to Razorpay Fund Account API to create a fund account for a contact
  async function razorpayPostFundAccBank(req) {
    const axios = require("axios");
    const key=config.key;
    const secret=config.secret;
    //JSON.stringify(payload);

    const payload = {
      contact_id: req.body.contact_id,
      account_type: req.body.account_type,
      bank_account:req.body.bank_account
    };


    try {
      var options = {
        method:'POST',
        url: 'https://'+key+':'+secret+'@api.razorpay.com'+'/v1/fund_accounts',
        data: payload
      };
      //console.log(options);
      const response = await axios(options);
     console.log("Response is "+ response.data);
      return response.data;
    } catch (err) {
      console.log("Error Response is ",  {status:err.response.status,statusText:err.response.statusText});
      return {status:err.response.status,statusText:err.response.statusText};
    }
  }
    //End of Fund account API call

    //Call to Razorpay Fund Account API to create a fund account for a contact
    async function razorpayPostFundAccVpa(req) {
      const axios = require("axios");
      const key=config.key;
      const secret=config.secret;
      //JSON.stringify(payload);

      const payload = {
        contact_id: req.body.contact_id,
        account_type: req.body.account_type,
        vpa:req.body.vpa,
        //payment_id:req.body.payment_id
      };


      try {
        var options = {
          method:'POST',
          url: 'https://'+key+':'+secret+'@api.razorpay.com'+'/v1/fund_accounts',
          //url: 'https://'+key+':'+secret+'@api.razorpay.com'+'/v1/payments/'+payload.payment_id+'/refund'
          data: payload
        };
        //console.log(options);
        const response = await axios(options);
       console.log("Response is "+ response.data);
        return response.data;
      } catch (err) {
        console.log("Error Response is ",  {status:err.response.status,statusText:err.response.statusText});
        return {status:err.response.status,statusText:err.response.statusText};
      }
    }
      //End of Fund account API call

async function customerEmailInsert(req) {
  const customerEmailData = {
    customer_id: req.body.customer_id,
    email_address: req.body.email_address,
    description: req.body.description,
    verified: req.body.verified,
    reserved: req.body.reserved,
    email_comm_ok: req.body.email_comm_ok
  };

  try {
    let message = "";
    const customerEmail1 = await customerEmail.query().insertGraph(
      customerEmailData
    );

    message = {
      statusCode: 200,
      body: {
        message: "New customer Email Created",
        id: customerEmail1.id
      }
    };

    console.log(message);
    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "New customer Email Errored" + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

async function customerPhoneInsert(req) {
  const customerPhoneData = {
    customer_id: req.body.customer_id,
    number: req.body.number,
    country_code: req.body.country_code,
    last4: req.body.last4,
    type: req.body.type,
    verified: req.body.verified,
    reserved: req.body.reserved
  };

  try {
    let message = "";
    const customerPhone1 = await customerPhone.query().insertGraph(
      customerPhoneData
    );

    message = {
      statusCode: 200,
      body: {
        message: "New customer Phone Created",
        id: customerPhone1.id
      }
    };

    console.log(message);
    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "New customer Phone Errored" + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

async function customerAddressInsert(req) {
  const customerAddressData = {
    customer_id: req.body.customer_id,
    line1:req.body.line1,
    line2:req.body.line2,
    city:req.body.city,
    zip_code:req.body.zip_code,
    state:req.body.state,
    country:req.body.country
  };

  try {
    let message = "";
    const customerAddress1 = await customerAddress.query().insertGraph(
      customerAddressData
    );

    message = {
      statusCode: 200,
      body: {
        message: "New customer Address Created",
        id: customerAddress1.id
      }
    };

    console.log(message);
    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "New customer Address Errored" + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

async function customerKYCDetailsInsert(req) {
  const customerKYCDetailsData = {
    customer_id: req.body.customer_id,
    document_type: req.body.document_type,
    document_unique_id: req.body.document_unique_id,
    description: req.body.description,
    verified: req.body.verified,
    document_id: req.body.document_id
  };

  try {
    let message = "";
    const customerKYCDetails1 = await customerKYCDetails.query().insertGraph(
      customerKYCDetailsData
    );

    message = {
      statusCode: 200,
      body: {
        message: "New customer KYC Details Created",
        id: customerKYCDetails1.id
      }
    };

    console.log(message);
    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "New customer KYC Details Errored" + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

async function creditsInsert(req) {
  const creditsInsertData = {
    name: req.body.name,
    description: req.body.description,
    maxCredit: req.body.maxCredit,
    cost: req.body.cost,
    expirationPolicy: req.body.expirationPolicy
  };

  try {
    let message = "";
    const instance = await credits.query().insertGraph(creditsInsertData);

    message = {
      statusCode: 200,
      body: {
        message: "New credits Created",
        id: instance.id
      }
    };

    console.log(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "New credits Errored" + e
    };
    console.log(message);
    return message;
  }
}

async function referralsInsert(req) {
  const referralsInsertData = {
    customer_id: req.body.customer_id,
    referral_code: req.body.referral_code
  };

  try {
    let message = "";
    const instance = await Referrals.query().insertGraph(referralsInsertData);

    message = {
      statusCode: 200,
      body: {
        message: "New referral Created",
        id: instance.id
      }
    };

    console.log(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "New referral Errored" + e
    };
    console.log(message);
    return message;
  }
}

async function userCreditsInsert(req) {
  const userCreditsData = {
    customer_id: req.body.customer_id,
    credit_id: req.body.credit_id,
    activation_date: req.body.activation_date,
    initial_credits: req.body.initial_credits,
    expiration_date: req.body.expiration_date,
    remaining_credits: req.body.remaining_credits,
    spent_credits: req.body.spent_credits
  };

  try {
    let message = "";
    const userCredits = await usercredits.query().insertGraph(userCreditsData);

    message = {
      statusCode: 200,
      body: {
        message: "New userCredits Created",
        id: userCredits.id
      }
    };

    console.log(message);
    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "New userCredits Errored" + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

async function sendEmailInvoice(req) {
  //console.log("Email data",emailData);
  const gigsta_config = require('../gigstaconfig');
  const emailData = {
    customer_id: req.body.customer_id,
    customer_name: req.body.customer_name,
    employee_id: req.body.employee_id,
    employee_name: req.body.employee_name,
    task_id: req.body.task_id,
    task_name: req.body.task_name,
    case_no:req.body.case_no,
    employee_email:req.body.employee_email,
    task_charges:req.body.task_charges
  };

console.log("Email data",emailData);
  try {
        let message = ""
        var nodemailer = require('nodemailer');

        var transporter = nodemailer.createTransport({
          service: 'gmail',
          auth: {
            user: gigsta_config.senderMail,
            pass: gigsta_config.senderMailPassword
          }
        });

        var mailOptions = {
          from: gigsta_config.senderMail,
          to: emailData.employee_email,
          subject: 'Gigsta - Invoice',
          html: '<!DOCTYPE html><html>\
          <body>\
          <p>Sender name : '+emailData.customer_name+'</p>\
          </br>\
          <p>Task name : '+emailData.task_name+'</p>\
          </br>\
          <p>Case No : '+emailData.case_no+'</p>\
          </br>\
          <p>Charges : '+emailData.task_charges+'</p>\
          </br>\
          <p>Thank you for the task. This is an invoice for the task provided. Please pay Rs.'+emailData.task_charges+'</p>\
          </br>\
          </body>\
          </html>'
        };

        transporter.sendMail(mailOptions, function(error, info){
          if (error) {
            console.log(error);
          } else {
            console.log('Email sent: ' + info.response);
          }
        });
    /*let message = "";
    var sendEmail = mailer.config({
        email: gigsta_config.senderMail,
        password: gigsta_config.senderMailPassword,
        server: gigsta_config.senderServer
    });
//console.log("sendEmail",sendEmail);
    var options = {
        subject: 'Gigsta - Invoice',
        senderName: emailData.customer_name,
        receiver: emailData.employee_email,
        html: '<!DOCTYPE html><html>\
        <body>\
        <p>Task name : '+emailData.task_name+'</p>\
        </br>\
        <p>Case No : '+emailData.case_no+'</p>\
        </br>\
        <p>Charges : '+emailData.task_charges+'</p>\
        </br>\
        <p>Thank you for the task. This is an invoice for the task provided. Please pay Rs.'+emailData.task_charges+'</p>\
        </br>\
        </body>\
        </html>'
    };


    message = {
      statusCode: 200,
      body: {
        message: "Email sent"
      }
    };

    console.log(message);
    //  res.json(message);
    return message;*/
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Email Errored" + e
    };
  }
  message = {
    statusCode: 200,
    body: {
      message: "Email sent"
    }
  };
    return message;
}

async function userReferralsInsert(req) {
  const { raw } = require('objection');
  const userReferralsData = {
    referral_id: req.body.referral_id,
    referred_to: req.body.referred_to,
  };
  current_date = new Date();
  try {
    let message = "";
    const userReferrals = await UserReferrals.query().insertGraph(userReferralsData);

   /*const customerGetReferralsData = await Referrals
      .query()
      .where('id', userReferralsData.referral_id);

     const referrer_id = customerGetReferralsData[0].customer_id;
//console.log("1. Referrer id",referrer_id);
     const creditsGetData = await credits
        .query()
        .where('id', 4);
        expiration_policy = creditsGetData[0].expirationPolicy;
        //console.log("2. Expiration policy",expiration_policy);
//Referree Credits Code
    const getCredits_referree = await usercredits.query()
     .where('customer_id',userReferralsData.referred_to)
     .where('credit_id', 4);
//console.log("3. getCredits_referree is null or not",getCredits_referree);
     /// Code for POST ot PUT to userCredits for referred_to
    if(getCredits_referree.length > 0)
    {
      //console.log("4. getCredits_referree is  not null");
      const userCredits_referree = await usercredits.query()
      .patch({
        initial_credits: getCredits_referree[0].remaining_credits,
        remaining_credits:raw('remaining_credits+5')
      })
      .where('customer_id',userReferralsData.referred_to)
      .where('credit_id', 4);
    }
    else{
      //console.log("4. getCredits_referree is null");
    const userCreditsRefereeData = {
      customer_id: userReferralsData.referred_to,
      credit_id: 4,
      activation_date: new Date(),
      initial_credits: 0,
      expiration_date: new Date(current_date.setMonth(current_date.getMonth() + creditsGetData[0].expirationPolicy)),
      remaining_credits: creditsGetData[0].maxCredit,
      spent_credits: 0
    };
    //console.log("5. userCreditsRefereeData is this",userCreditsRefereeData);
    const userCreditsReferee = await usercredits.query().insertGraph(userCreditsRefereeData);
    //console.log("6. userCreditsReferee is inserted",userCreditsReferee);
  }
//Referrer Credits code
    const getCredits_referrer = await usercredits.query()
    .where('customer_id',referrer_id)
    .where('credit_id', 4);
//console.log("7. getCredits_referrer is null or not",getCredits_referrer);
//console.log(" Length of getCredits_referrer", getCredits_referrer.length);
    //if(Object.getOwnPropertyNames(getCredits_referrer).length > 0)
    if(getCredits_referrer.length > 0)
    {
    //  console.log("8. getCredits_referrer is  not null");
      const userCredits_referrer = await usercredits.query()
      .patch({
        initial_credits: getCredits_referrer[0].remaining_credits,
        remaining_credits:raw('remaining_credits+5')
      })
      .where('customer_id',referrer_id)
      .where('credit_id', 4);
    }
    else{
      //console.log("9. getCredits_referrer is null");
    const userCreditsReferrerData = {
      customer_id: referrer_id,
      credit_id: 4,
      activation_date: new Date(),
      initial_credits: 0,
      expiration_date: new Date(current_date.setMonth(current_date.getMonth() + creditsGetData[0].expirationPolicy)),
      remaining_credits: creditsGetData[0].maxCredit,
      spent_credits: 0
    };
  //  console.log("10. userCreditsRefererData is this",userCreditsReferrerData);
    const userCreditsReferrer = await usercredits.query().insertGraph(userCreditsReferrerData);
  //  console.log("6. userCreditsReferer is inserted",userCreditsReferrer);
}*/


    message = {
      statusCode: 200,
      body: {
        message: "New userReferrals Created",
        id: userReferrals.id
      }
    };

    console.log(message);
    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "New userReferrals Errored" + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}


async function referralGenerator(req) {
  var firstPart = (Math.random() * 46656) | 0;
  var secondPart = (Math.random() * 46656) | 0;
  firstPart = ("000" + firstPart.toString(36)).slice(-3);
  secondPart = ("000" + secondPart.toString(36)).slice(-3);
  return firstPart + secondPart;
}

async function customerPaymentsInsert(req) {
  const customerPaymentsData = {
    customer_id: req.body.customer_id,
    payment_type: req.body.payment_type,
    payment_unique_id: req.body.payment_unique_id,
    verified: req.body.verified,
    description: req.body.description,
    document_id: req.body.document_id,
    fund_account_id:req.body.fund_account_id
  };

  try {
    let message = "";
    const customerPayments1 = await customerPayments.query().insertGraph(
      customerPaymentsData
    );

    message = {
      statusCode: 200,
      body: {
        message: "New customer Payments Created",
        id: customerPayments1.id
      }
    };

    console.log(message);
    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "New customer Payments  Errored" + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

async function sendInvoiceSes(req) {
  const gigstaconfig = require('../gigstaconfig');

  const emailData = {
    customer_id: req.body.customer_id,
    customer_name: req.body.customer_name,
    employee_id: req.body.employee_id,
    employee_name: req.body.employee_name,
    task_id: req.body.task_id,
    task_name: req.body.task_name,
    case_no:req.body.case_no,
    employee_email:req.body.employee_email,
    task_charges:req.body.task_charges
  };

  const AWS = require('aws-sdk');

  // Amazon SES configuration
  const SESConfig = {
    apiVersion: '2010-12-01',
    accessKeyId: config.accessKeyId,
    secretAccessKey: config.secretAccessKey,
    region: config.region
  };
  console.log("Ses config",SESConfig);
//  console.log("sendMail",gigstaconfig);

  var params = {
    Source:  gigsta_config.senderMail,
    Destination: {
      ToAddresses: [
        emailData.employee_email
      ]
    },
    ReplyToAddresses: [
      gigstaconfig.sendMail,
    ],
    Message: {
      Body: {
        Html: {
          Charset: "UTF-8",
          Data: '<html>\
          <body>\
          <p>Sender name : '+emailData.customer_name+'</p>\
          </br>\
          <p>Task name : '+emailData.task_name+'</p>\
          </br>\
          <p>Case No : '+emailData.case_no+'</p>\
          </br>\
          <p>Charges : '+emailData.task_charges+'</p>\
          </br>\
          <p>Thank you for the task. This is an invoice for the task provided. Please pay Rs.'+emailData.task_charges+'</p>\
          </br>\
          </body>\
          </html>'
        }
      },
      Subject: {
        Charset: 'UTF-8',
        Data: 'Gigsta - Invoice'
      }
    }
  };

  var sendPromise = new AWS.SES(SESConfig).sendEmail(params).promise();

  sendPromise.then(
  function(data) {
    console.log(data.MessageId);
  }).catch(
    function(err) {
    console.error(err, err.stack);
  });
}

module.exports.customerInsert = customerInsert;
module.exports.razorpayPostContact = razorpayPostContact;
module.exports.razorpayPostFundAccBank = razorpayPostFundAccBank;
module.exports.razorpayPostFundAccVpa = razorpayPostFundAccVpa;
module.exports.customerProfileInsert = customerProfileInsert;
module.exports.customerPaymentsInsert = customerPaymentsInsert;
module.exports.customerKYCDetailsInsert = customerKYCDetailsInsert;
module.exports.customerAddressInsert = customerAddressInsert;
module.exports.customerPhoneInsert = customerPhoneInsert;
module.exports.creditsInsert = creditsInsert;
module.exports.userCreditsInsert = userCreditsInsert;
module.exports.customerEmailInsert = customerEmailInsert;
module.exports.userReferralsInsert = userReferralsInsert;
module.exports.referralsInsert = referralsInsert;
module.exports.referralGenerator = referralGenerator;
module.exports.sendEmailInvoice = sendEmailInvoice;
module.exports.sendInvoiceSes = sendInvoiceSes;
