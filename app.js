const bodyParser = require("body-parser");

const cookieParser = require("cookie-parser");

let app = require("express")();

//let taskSearch = require("./controllers/taskSearch");
let customer = require("./controllers/customer");
let address = require("./controllers/address");
let email = require("./controllers/email");
let kycDetails = require("./controllers/kycDetails");
let payments = require("./controllers/payments");
let phone = require("./controllers/phone");
let profile = require("./controllers/profile");
let credits = require("./controllers/credits");
let referrals = require("./controllers/referrals");
let userCredits = require("./controllers/usercredits");
let userReferrals = require("./controllers/userReferrals");

//let instance = require("./controllers/instance");

const { Model } = require("objection");
const knex = require("knex");

const KnexConfig = require("./knexfile");

Model.knex(knex(KnexConfig.development));
//body and cookie parsers

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
// Add headers
app.use(function(req, res, next) {
  // Website you wish to allow to connect
  res.setHeader("Access-Control-Allow-Origin", "*");

  // Request methods you wish to allow
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, OPTIONS, PUT, PATCH, DELETE"
  );

  // Request headers you wish to allow
  res.setHeader(
    "Access-Control-Allow-Headers",
    "X-Requested-With,content-type"
  );

  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res.setHeader("Access-Control-Allow-Credentials", true);

  // Pass to next layer of middleware
  next();
});

app.all("/", function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "X-Requested-With");
  next();
});

//Initialize routing dependencies

//app.use("/task/search", taskSearch);
app.use("/customer/data/", customer);
app.use("/customer/address/", address);
app.use("/customer/email/", email);
app.use("/customer/kycDetails/", kycDetails);
app.use("/customer/payments/", payments);
app.use("/customer/phone/", phone);
app.use("/customer/profile/", profile);
app.use("/customer/credits/", credits);
app.use("/customer/referrals/", referrals);
app.use("/customer/userReferrals/", userReferrals);
app.use("/customer/userCredits/", userCredits);

//app.use("/task/instance", instance);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  let err = new Error("Not Found");
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  console.log(err);
  res.send("error");
});

/*
app.listen(process.env.port||"7000");
console.log("listening on port 7000")
*/
module.exports = app;
